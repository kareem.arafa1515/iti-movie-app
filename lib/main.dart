import 'package:flutter/material.dart';
import 'package:iti_movie_app/screens/movie/UI/movie_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Movie App',
      debugShowCheckedModeBanner: false,
      home: MovieView(),
      theme: ThemeData.dark(),
    );
  }
}

