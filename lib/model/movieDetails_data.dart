

class DetailsModel {
  String overview;
  String title;
  List<Genres> genres;
  String posterPath;
  String backdropPath;
  String releaseDate;
  num voteAverage;
  int id;

  DetailsModel.details();

  DetailsModel({
    this.backdropPath,
    this.id,
    this.overview,
    this.posterPath,
    this.releaseDate,
    this.title,
    this.voteAverage,
  });

  DetailsModel.fromJSON(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'] ?? 0;
    voteAverage = parsedJson['vote_average'] ?? 0.0;
    releaseDate = parsedJson['release_date'] ?? "";
    backdropPath = parsedJson['backdrop_path'] ?? '';
    posterPath = parsedJson['poster_path'] ?? '';
    title = parsedJson['title'] ?? '';
    overview = parsedJson['overview'] ?? '';
    List<Genres> temp = [];

    for (int i = 0; i < parsedJson['genres'].length; i++) {
      Genres result = Genres(parsedJson['genres'][i]);
      temp.add(result);
    }
    genres = temp;
  }
}

class Genres {
  int id;
  String name;

  Genres(genres) {
    id = genres['id'];
    name = genres['name'];
  }

}
