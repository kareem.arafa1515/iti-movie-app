import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:iti_movie_app/model/moviemodel_data.dart';

import 'movieScreen.dart';

class MovieView extends StatefulWidget {

  MovieView({Key key}) : super(key: key);

  @override
  _MovieViewContentState createState() => _MovieViewContentState();
}

class _MovieViewContentState extends State<MovieView>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TabController _tabController;


  @override
  void initState() {
    _tabController = new TabController(length: 4, vsync: this);
    super.initState();
  }

  @override
  void didUpdateWidget(MovieView oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {});
  }

  void showError(String error) {
    final snackBar = SnackBar(content: Text(error));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return homeView();
  }

  Widget homeView (){
        return appView();
  }

  Widget appView(){
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie app"),
        actions: <Widget>[
          FlatButton(
            child: Text('Favorite'),
            onPressed: (){
              Navigator.pushNamed(context,'Favorite');
            },
          )
        ],
        leading: Icon(
          Icons.home,
          color: Colors.white,
        ),
        bottom: TabBar(
          tabs: [
            Tab(text: 'Popular'),
            Tab(text: 'Now playing'),
            Tab(text: 'top rated'),
            Tab(text: 'Upcoming'),
          ],
          controller: _tabController,
          indicatorColor: Colors.amber,
          unselectedLabelColor: Colors.white,
          labelColor: Colors.amber,
          indicatorSize: TabBarIndicatorSize.tab,
        ),
        bottomOpacity: 1,
      ),
      key: _scaffoldKey,
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          MovieScreen(type: describeEnum(MovieType.popular)),
          MovieScreen(type: describeEnum(MovieType.now_playing)),
          MovieScreen(type: describeEnum(MovieType.top_rated)),
          MovieScreen(type: describeEnum(MovieType.upcoming)),
        ],
      ),
       //buildCustomScrollView(),
    ); 
  }
}
