import 'package:flutter/material.dart';
import 'package:iti_movie_app/model/moviemodel_data.dart';
import 'package:iti_movie_app/remote/moviemodel_repository.dart';
import 'package:iti_movie_app/screens/movie/UI/movieDetails_view.dart';

class MovieScreen extends StatefulWidget {
  final String type;

  MovieScreen({Key key, this.type}) : super(key: key);

  @override
  _PopularMovieState createState() => _PopularMovieState();
}

class _PopularMovieState extends State<MovieScreen> {
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  Widget getPageContent() {
    return FutureBuilder(
        future: MovieModelRepository().getNowMovieModelsList(widget.type),
        builder: (context, movieSnapsShot) {
          if (movieSnapsShot.connectionState == ConnectionState.waiting &&
              !movieSnapsShot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else if (movieSnapsShot.hasError) {
            return Center(child: Text("Error"));
          } else {
            return GridView.builder(
              itemCount: movieSnapsShot.data.length,
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2),
              itemBuilder: (BuildContext context, int index) {
                MovieModel movieModel = movieSnapsShot.data[index];
                return GridTile(
                  child: InkResponse(
                    enableFeedback: true,
                    child: Image.network(
                      'https://image.tmdb.org/t/p/w200${movieModel.posterPath}',
                      fit: BoxFit.fill,
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return MovieDetailsView(movieModel.id);
                      }));
                    },
                  ),
                );
              },
              controller: _scrollController,
            );
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return getPageContent();
  }
}
