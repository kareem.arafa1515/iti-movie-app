import 'dart:async';

import 'package:dio/dio.dart';
import 'package:iti_movie_app/model/movieDetails_data.dart';



class MovieDetailsRepository {

  MovieDetailsRepository();
  final _apiKey = "f55fbda0cb73b855629e676e54ab6d8e";
  final Dio dio = Dio();

  Future<DetailsModel> getMovieDetails(int id) {
    print("repository :$id");
    return  dio.get("http://api.themoviedb.org/3/movie/${id.toString()}", queryParameters: {
      "api_key": _apiKey,
    }).then((d) {
print(d.data);
      return DetailsModel.fromJSON(d.data);
    });
  }
}