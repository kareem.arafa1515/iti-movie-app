import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:iti_movie_app/model/moviemodel_data.dart';

class MovieModelRepository {
  MovieModelRepository();

  final _apiKey = "f55fbda0cb73b855629e676e54ab6d8e";
  final Dio dio = Dio();
  final JsonDecoder _decoder = new JsonDecoder();

  Future<List<MovieModel>> getNowMovieModelsList(String type) async {
    return dio.get('http://api.themoviedb.org/3/movie/$type', queryParameters: {
      "api_key": _apiKey,
    }).then((d) {
      print(d.toString());

      return (d.data['results'] as List).map((movie) => MovieModel.fromJson(movie)).toList();
    });
  }

  Future<MovieModel> createMovieModel(MovieModel moviemodel) {
    dio.options.headers.putIfAbsent("Accept", () {
      return "application/json";
    });
    return dio
        .post("http://api.themoviedb.org/3/movie/moviemodel/", data: moviemodel)
        .then((d) {

     return new MovieModel.fromJson(d as Map);
    });
  }

  Future<MovieModel> updateMovieModel(MovieModel moviemodel) {
    dio.options.headers.putIfAbsent("Accept", () {
      return "application/json";
    });
    return dio
        .put("http://api.themoviedb.org/3/movie/moviemodel/", data: moviemodel)
        .then((d) {
      return new MovieModel.fromJson(d as Map);
    });
  }

  Future<MovieModel> getMovieModel(int id) {
    return dio.get("http://api.themoviedb.org/3/movie/moviemodel/",
        queryParameters: {"id": id}).then((d) {
      return new MovieModel.fromJson(d as Map);
    });
  }
}
